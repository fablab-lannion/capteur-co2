#include <Arduino.h>
#include <CCS811.h>
#include <Adafruit_NeoPixel.h>
#include <queue>
#include <deque>
#include <iostream>
#include <EEPROM.h>

//
// Parameters
//

// Pin of the Neopixel Led RIng
#define PIN       D4
// Quantity of pixels in the ring
#define NUMPIXELS 12
// Number of values in the history
#define NB_MESURES 60
// Time of a led off for a nice display
#define TIME_LED_OFF 50
// Number of blinks at level change
#define STATE_CHANGE_BLINKS 10
// Time of a blink at level change
#define BLINK_DELAY 1000
// Daily history granularity - store one value on
#define HIST_SLOT 15*60
// Daily history size
#define HIST_SIZE 1440/HIST_SLOT
// Minimal sensor value
#define MIN 400
// Maximum value
#define MAX 2100
// Default baseline
#define DEFAULT_BASELINE 20000

/* 
FixedQueue object.

Queue object of fixed size with mean function.
This object will allow us to have an history of last values.
*/
template <typename T, int MaxLen, typename Container=std::deque<T>>
class FixedQueue : public std::queue<T, Container> {
  public:
    // We overload push to ensure the limit of the queue 
    void push(const T& value) {
      if (this->isfull()) {
        this->c.pop_front();
      }
      std::queue<T, Container>::push(value);
    };
    // Calc mean of the values in the queue
    uint16_t mean() {
      uint32_t sum = 0;
      for (int i=0; i<MaxLen; i++){
        sum += this->c[i];
      }
      return int(sum/MaxLen);
    };
    // Answer if history is full
    bool isfull(){
      return (this->size() >= MaxLen);
    };
    // Return minimal value
    uint16_t min(){
      uint16_t min=MAX;
      for (int i=0; i < int(this->size()); i++){
        if (this->c[i] < min){
          min = this->c[i];
        };
      };
      return min;
    };
};

//
// Objects declarations section
//

Adafruit_NeoPixel leds = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
//CCS811 sensor(&Wire, /*IIC_ADDRESS=*/0x5A);
CCS811 sensor;

enum ALL_STATES { EXCELLENT, GOOD, FAIR, MEDIOCRE, BAD, VERY_BAD, UNKNOWN } ;

FixedQueue<uint16_t, NB_MESURES> history;
FixedQueue<uint16_t, NB_MESURES> baseline_history;
FixedQueue<uint16_t, HIST_SIZE> daily_baseline;

int daily_history_cnt = 0;

//
// Functions section
//

// Return a list of 3 int corresponding to RGV color values to display for
// a specific state.
std::array<int, 3> get_state_color(byte state){
  switch (state){
    case EXCELLENT:
      return {0, 255, 0};
      break;
    case GOOD:
      return {32, 255, 0};
      break;
    case FAIR:
      return {140, 255, 0};
      break;
    case MEDIOCRE:
      return {255, 230, 0};
      break;
    case BAD:
      return {255, 80, 0};
      break;
    case VERY_BAD:
      return {255, 0, 0};
      break;
    case UNKNOWN:
      return {64, 64, 64};
      break;
    default:
      return {255, 255, 255};
  }
}

// Display the color corresponding to the ppm value
// This is here we set the level corresponding to ppm
byte get_state(uint16_t ppm_value) {
  // scale src: https://images.staticjw.com/vcp/6394/co2-ppm-table.jpg
  // 400 -600: Excellent - bright green
  // 700-800: Good
  // 900-1000: Fair
  // 1100-1500: Mediocre - Yellow
  // 1600-2000: Bad - Orange
  // > 2000: very bad
  if ( 2000 < ppm_value && ppm_value < 5000){
    Serial.println("State: VERY_BAD");
    return VERY_BAD;
  } else if ( 1600 < ppm_value && ppm_value < 2000){
    Serial.println("State: BAD");
    return BAD;
  } else if ( 1100 < ppm_value && ppm_value < 1600){
    Serial.println("State: MEDIOCRE");
    return MEDIOCRE;
  } else if ( 900 < ppm_value && ppm_value < 1100){
    Serial.println("State: FAIR");
    return FAIR;
  } else if ( 700 < ppm_value && ppm_value < 900){
    Serial.println("State: GOOD");
    return GOOD;
  } else if ( MIN <= ppm_value && ppm_value < 700){
    Serial.println("State: EXCELLENT");
    return EXCELLENT;
  };
  Serial.println("State: UNKNOWN");
  return UNKNOWN;
};

// Return the CO2 sensor value.
uint16_t get_sensor_value(){
  // As the minimal value of the sensor is 400 return it if not ready
  uint16_t value = 400;
  if(sensor.checkDataReady() == true){
    Serial.print("Sensor :: CO2: ");
    Serial.print(sensor.getCO2PPM());
    Serial.print("ppm - TVOC: ");
    Serial.print(sensor.getTVOCPPB());
    Serial.println("ppb"); 
    value = sensor.getCO2PPM();     
  } else {
    Serial.println("Data is not ready!");
  }
  return value;
}

// Display a level on leds
// the value must be between 0 (for all off) to NUMPIXELS for all on
void display_pix(int nb_pix, byte level){
  std::array<int, 3> color = get_state_color(level);
  for(int i=0;i<nb_pix;i++){
    leds.setPixelColor(i, leds.Color(color[0], color[1], color[2]));
  }
  for(int i=nb_pix;i<NUMPIXELS;i++){
    leds.setPixelColor(i, leds.Color(0, 0, 0));
  }
  leds.show();
}

// Display a level on leds
// the value must be between 0 (for all off) to NUMPIXELS for all on
void display_level(uint16_t value){
  Serial.printf("Mean floating value: %u\n", value);
  int nb_pix = uint16_t((value - MIN) * (NUMPIXELS) / (MAX - MIN))+1;
  display_pix(nb_pix, get_state(value));
}

// Setup the board
void setup(void)
{
  Serial.begin(115200);
  /*Wait for the chip to be initialized completely, and then exit*/
  while(sensor.begin() != 0){
    Serial.println("failed to init chip, please check if the chip connection is fine");
    delay(1000);
  }
  leds.begin();
  /**
   * @brief Set measurement cycle
   * @param cycle:in typedef enum{
   *                  eClosed,      //Idle (Measurements are disabled in this mode)
   *                  eCycle_1s,    //Constant power mode, IAQ measurement every second
   *                  eCycle_10s,   //Pulse heating mode IAQ measurement every 10 seconds
   *                  eCycle_60s,   //Low power pulse heating mode IAQ measurement every 60 seconds
   *                  eCycle_250ms  //Constant power mode, sensor measurement every 250ms
   *                  }eCycle_t;
   */
  sensor.setMeasCycle(sensor.eCycle_1s);
  for (uint16_t i=MIN; i<MAX; i++){
    display_level(i);
    delay(BLINK_DELAY/500);
  }
  display_pix(NUMPIXELS, EXCELLENT);
  delay(BLINK_DELAY);
  display_pix(NUMPIXELS, GOOD);
  delay(BLINK_DELAY);
  display_pix(NUMPIXELS, FAIR);
  delay(BLINK_DELAY);
  display_pix(NUMPIXELS, MEDIOCRE);
  delay(BLINK_DELAY);
  display_pix(NUMPIXELS, BAD);
  delay(BLINK_DELAY);
  display_pix(NUMPIXELS, VERY_BAD);
  delay(BLINK_DELAY);
  display_pix(NUMPIXELS, UNKNOWN);
  delay(BLINK_DELAY);
  Serial.print("Init led done\n");
  // init values history with minimal value of the sensor 
  for (int i = 0; i < NB_MESURES+1; i++){
    history.push(MIN);
  }
  // init baseline history with default baseline 
  for (int i = 0; i < NB_MESURES+1; i++){
    baseline_history.push(DEFAULT_BASELINE);
  }
  // Get baseline from eeprom
  if (EEPROM.read(0) == 0){
    Serial.printf("Use default Baseline: %i\n", DEFAULT_BASELINE);
    sensor.writeBaseLine(DEFAULT_BASELINE);
  }else{
    sensor.writeBaseLine(EEPROM.read(0));
    Serial.printf("Use EEPROM Baseline: %i\n", EEPROM.read(0));
  };
}

void baseline_mgmt(){
  uint16_t baseline = baseline_history.mean();
  Serial.printf("Save mean baseline %u\n", baseline);
  daily_baseline.push(baseline);
  if (daily_baseline.isfull()){
    uint16_t min = daily_baseline.min();
    Serial.printf("Set baseline to min value of the day: %u\n", min);
    sensor.writeBaseLine(min);
    // save it to eeprom
    EEPROM.write(0, min);
    daily_baseline.empty();
  }
}

// Loop routine
void loop() {
  history.push(get_sensor_value());
  baseline_history.push(sensor.readBaseLine());
  Serial.printf("Mean floating baseline: %u\n", baseline_history.mean());
  /*!
    * @brief Set baseline
    * @param get from getBaseline.ino
    */
  delay(1000);
  display_level(history.mean());
  // Baseline mgmt
  daily_history_cnt++;
  if (daily_history_cnt >= HIST_SLOT){
    daily_history_cnt = 0;
    baseline_mgmt();
  }
}