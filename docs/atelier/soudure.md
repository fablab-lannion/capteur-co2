# Atelier soudure

## Un peu de théorie

On va relier les composants entre eux en réalisant quelques soudures.
Pour ceux qui se souviennent de leur cours de techno, ça revient à
mettre des fils entre différents points des composants.

Un petit [tuto soudure](https://www.framboise314.fr/comment-bien-souder-un-tutoriel-sur-la-soudure/)
sur la question.

On considère les composants à relier

![soudure1](./images/archi2.png)

si on zoome sur le composants on voit qu'il existe des terminaisons
particulières G, GND, 5V, D1, D2, D4, SDA, DI, 3V3, TX...

Le choix des soudures est lié aux composants et à l'implémentation
logicielle.

SI on retourne les leds on voit aussi 4 terminaisons: DI, 5V, GND, DO.

![revers_leds](./images/leds-2.jpg)

D1 et D2 pour le capteur sont imposés car ces points ont des caractéristiques
particulières (connection I2C).

D4 est un choix arbitraire qu'on a fait dans le code qu'on a téléversé.

```cpp
// Pin of the Neopixel Led RIng
#define PIN       D4
```

## Cablage pratique

Reprenons le schéma précédent en mettant en évidence les points de connection
utiles à notre objet.

![soudure2](./images/cablage.png)

Commençons par relier le capteur à l'élément central

![soudure3](./images/cablage-capteur.png)

Puis relions les leds

![soudure4](./images/cablage-led.png)

Au final l'ensemble des connections ressemblent à ça

![soudure5](./images/cablage-complet.png)

On voit que certains fils finissent sur le même point.

La plupart des soudures ont été faites en avance car ça prend un peu
de temps.

Pour faciliter les soudures, on utilise des "nappes" (ensemble de fils).
Pour l'objet on utilise une nappe de 5/6 cm  entre le wemos et les leds et
une nappe de 4/5cm entre le wemos et le capteur.

![soudure6](./images/soudure-01.jpg)
![soudure7](./images/soudure-02.jpg)
![soudure8](./images/soudure-04.jpg)
![soudure9](./images/soudure-05.jpg)
