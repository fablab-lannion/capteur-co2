# Atelier L'objet capteur

Il s'agit de réaliser la "boîte" qui va contenir toute l'électronique.

Dans l'absolu vous pourriez totalement personnaliser cette "boîte".

Plusieurs prototypes ont d'ailleurs été réalisés.
<img src="https://wiki.fablab-lannion.org/images/6/6a/20211204_184708.jpg"  width="120">

La lecture de la documentation du capteur a conduit à réaliser 
un objet compact et solide, composé de plusieurs couches que l'on
va découper avec précision grâce à la découpeuse laser.

Les plans:  ![plans](../../objet/support2_v2.svg)

## Du dessin..

Le plan est un fichier svg (Standard Vector Graphics ), c'est un
format de dessin vectoriel (par opposition au dessin par pixel).

Il existe de nombreux logiciels permettant de faire ce genre de dessins.

Au fablab nous privilégions les logiciels libres, le logiciel de référence
pour le dessin vectoriel s'appelle [inkscape](https://inkscape.org).

Nous avons d'ailleurs fait des sessions de formation à cet outil.
Il existe de nombreux tutoriels en ligne.

Cerise sur le gateau numérique, ce logiciel peut également servir pour la
brodeuse numérique.

## ...A la découpe Laser

Comment passe-t-on du dessin à la machine?
Une fois n'est pas coutume tout est histoire de format.

La découpeuse Laser comprend différents formats dont un format appelé
DXF (Drawing eXchange Format). Inkscape permet d'exporter un dessin
en format DXF. Or le logiciel RDWorks qui pilote la machine permet d'importer
le dessin en DXF. Ce logiciel se trouve sur le PC juste à côté de la machine.

Le logiciel va dessiner et selon la couleur du trait et la configuration du
logiciel,  il le fera plus ou moins vite et avec plus ou moins de puissance
au niveau de Laser afin d'obtenir le rendu attendu en fonction du matériau
(découpe, gravure).

Plus d'informations sur la machine sur le
[wiki du fablab](https://wiki.fablab-lannion.org/index.php?title=Découpeuse_laser_Keyland).

## Collage

L'objet a été dessiné en couche. Une fois toutes les couches découpées, il faut
simplement les coller avec de la colle à bois.

## Diffuseur de leds

Afin d'améliorer la diffusion de la lumière des leds, on couvre les leds
aved des petits bouts de plexiglas, découpés, eux-aussi avec la découpeuse
laser.

## Carter

Afin d'avoir un objet au rendu plus fini, on a choisi de rajouter un petit
carter en plastique.

Plusieurs techniques ont été envisagées:

* le [thermoformage](https://wiki.fablab-lannion.org/index.php?title=Thermoformeuse)
  concrêtement faire un moule et mouler du appliquer une feuille de plastique
  sur ce moule
* [l'impression 3D](https://wiki.fablab-lannion.org/index.php?title=PrusaI3_MK3S)

Nous avons choisi l'impression 3D car ça coute un peu moins cher et la pièce est
relativement simple.

Une fois encore nous utilisons la découpeuse laser pour découper les petits
trous correspondant aux leds et à la grille qui permet à l'air de traverser
le capteur.

## Intégration

L'opération consiste à intégrer l'électronique complet à l'intérieur de
l'objet en bois.

la dernière étape consiste à rajouter le petit carter sur l'objet et les
diffuseurs de lumière.



