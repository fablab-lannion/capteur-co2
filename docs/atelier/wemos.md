# Atelier Wemos

## présentation du Wemos

Le Wemos est le cerveau de l'objet.
Il va récupérer les informations du capteur, les convertir et
piloter l'affichage en fonction des valeurs.

Une petite présentaiton du Wemos D1 mini
[Wemos D1 mini](https://www.aranacorp.com/fr/vue-densemble-du-microcontroleur-wemos-d1-mini/).

Ce Wemos peut faire beaucoup de choses, et si vous regardez la page
des projets du fablab, vous le reverrez souvent.

Dans le cadre de notre projet nous n'utiliserons que 3 points de connections
appelés pins (D1, D2 et D4) en plus des pins d'alimentation (5V et GND).
On n'utilise pas la puce wifi intégrée.

On peut comparer le Wemos à un petit ordinateur.

Pour l'utiliser, il faut créer un programme...
Rassurez-vous, ce programme est déjà écrit, il est disponible sur
le [site du fablab](https://gitlab.com/fablab-lannion/capteur-co2).

Il y a plusieurs méthodes pour téléverser le logiciel dans le Wemos.
On a choisi d'utiliser
[platform.io](https://www.youtube.com/watch?v=1bIQAHWXdfo).

Sur les PCs vous devez pouvoir ouvrir le logiciel Visual Studio Code

![vscode](./images/vscode.png)

## Zoom sur le code

Le code est écrit dans un language particulier (C++).
Le but n'est pas d'apprendre ce language de programmation mais de
comprendre globalement comment ça fonctionne...

Le fichier principal est le fichier [src/main.cpp](https://gitlab.com/fablab-lannion/capteur-co2/-/raw/main/src/main.cpp)

On va détailler un peu.

### Les librairies externes

Le capteur mais aussi les leds ont besoin de code pour fonctionner, ce code est fourni
à travers des librairies (code pré à l'emploi fourni par des tiers, généralement les
fabricants des composants).

```cpp
#include <Arduino.h>
#include <CCS811.h>
#include <Adafruit_NeoPixel.h>
#include <queue>
#include <deque>
#include <iostream>
#include <EEPROM.h>
```

ainsi CCS811 correspond au capteur de CO2, Adafruit_NeoPixel.h au
gestionaire de leds, Arduino.h au code de base,...

Il faut juste retenir qu'on n'a pas eu à écrire ce code là, on peut
le récupérer et l'intégrer sinmplement pour le réutiliser.

### les déclarations

On doit paramtérer/configurer un certain nombre de valeurs.
Cette opération se fait au début du fichier.

```cpp
// Pin of the Neopixel Led RIng
#define PIN       D4
// Quantity of pixels in the ring
#define NUMPIXELS 12
// Number of values in the history
#define NB_MESURES 60
// Time of a led off for a nice display
#define TIME_LED_OFF 50
// Number of blinks at level change
#define STATE_CHANGE_BLINKS 10
// Time of a blink at level change
#define BLINK_DELAY 1000
// Daily history granularity - store one value on
#define HIST_SLOT 15*60
// Daily history size
#define HIST_SIZE 1440/HIST_SLOT
// Minimal sensor value
#define MIN 400
// Maximum value
#define MAX 2100
// Default baseline
#define DEFAULT_BASELINE 20000
```

On y définit entre autre:

- le nombre de leds à piloter (la librairie est générique et permet de
  gérer de 1 à N leds)
- sur quelle PIN (point de connection) on va piloter les leds (ici le D4)
- tout un tas de valeur de temps pour savoir combien de temps on attend
  entre chaque mesure, des valeurs de référence...

C'est aussi dans cette section qu'on déclarer les composants, ici les leds
(Adafruit_NeoPixel) et le capteur (CCS811).

```bash
Adafruit_NeoPixel leds = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
CCS811 sensor;
```

### Les fonctions

Les fonctions sont les opérations que nous devons régulièrement appeler
pour faire fonctionner l'objet.

Les fonctions sont:

- get_state_color: elle fait la correspondance entre une couleur et
  la valeur à transmettre aux leds
- get_state: elle effectue la correspondance entre la valeur du capteur
  et la lumière désirée
- get_sensor_value: cette fonction récupère la valeur du capteur
- display_pix: cette fonction permet de piloter les leds
- setup: fonction d'initialisation
- loop: fonction de boucle qui va effectuer les différentes opérations
  en boucle...

Par exemple la fonction qui fait la correspondance entre la valeur du
capteur et la lumière est "codée" comme suit:

```cpp
byte get_state(uint16_t ppm_value) {
  // scale src: https://images.staticjw.com/vcp/6394/co2-ppm-table.jpg
  // 400 -600: Excellent - bright green
  // 700-800: Good
  // 900-1000: Fair
  // 1100-1500: Mediocre - Yellow
  // 1600-2000: Bad - Orange
  // > 2000: very bad
  if ( 2000 < ppm_value && ppm_value < 5000){
    Serial.println("State: VERY_BAD");
    return VERY_BAD;
  } else if ( 1600 < ppm_value && ppm_value < 2000){
    Serial.println("State: BAD");
    return BAD;
  } else if ( 1100 < ppm_value && ppm_value < 1600){
    Serial.println("State: MEDIOCRE");
    return MEDIOCRE;
  } else if ( 900 < ppm_value && ppm_value < 1100){
    Serial.println("State: FAIR");
    return FAIR;
  } else if ( 700 < ppm_value && ppm_value < 900){
    Serial.println("State: GOOD");
    return GOOD;
  } else if ( MIN <= ppm_value && ppm_value < 700){
    Serial.println("State: EXCELLENT");
    return EXCELLENT;
  };
  Serial.println("State: UNKNOWN");
  return UNKNOWN;
};
```

La fonction loop peut être résumée comme ceci:

```cpp
void loop() {
   je récupère la valeur du capteur
   je la garde en mémoire
   j'attends un peu
   je commande l'affichage en fonction de la valeur moyennée
   si c'est possible, je recalibre
```

Ces actions, réalisées par des fonctions se traduisent par le code suivant:

```cpp
void loop() {
  history.push(get_sensor_value());
  baseline_history.push(sensor.readBaseLine());
  Serial.printf("Mean floating baseline: %u\n", baseline_history.mean());
  /*!
    * @brief Set baseline
    * @param get from getBaseline.ino
    */
  delay(1000);
  display_level(history.mean());
  // Baseline mgmt
  daily_history_cnt++;
  if (daily_history_cnt >= HIST_SLOT){
    daily_history_cnt = 0;
    baseline_mgmt();
  }
  ```

## Téléversement

Il se fait via un cable USB depuis le PC en passant par le logiciel.

Si d'aventure vous souhaitez personnaliser le code, vous le pouvez.
Il faudra modifier le fichier, le compiler puis le téléverser.
