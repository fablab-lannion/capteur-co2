# Atelier de construction d'un détecteur de CO2

## Introduction

Cet atelier vise à permettre la réalisation d'un capteur de CO2.
Aucune connaissance n'est requise, lors de l'après midi, on
découvrira les resources du fablab à commencer par celle qui va permettre de ne
pas avoir de pré-requis...l'entraide.

Les animateurs de l'atelier sont:

* Florian
* Tangi
* Morgan

Lors de cet atelier, nous aurons l'occasion de découvrir ce lieu et d'utiliser
quelques machines.

## Présentation générale de l'objet

Le but de l'après midi est de construire en mode DIY (Do It Yourself - Fais
le toi-même) un capteur de CO2.
Il s'agit d'un appareil qui mesure la concentration en CO2 dans l'air et
fournit un affichage visuel de cette concentration.
Dès lors que la concentration dépasse un certain seuil, il est fortement
recommandé d'aérer.

Ces objets sont devenus assez populaires du fait de la pandémie de COVID-19.
Ils existaient bien avant mais sont devenus des objets recherchés.

Les prix sur le marché sont extrêmement variables en fonction de la qualité
des composants et des matériaux.

Aujourd'hui on va tenter de construire un objet assez simple mais robuste.

Des études ont permis de définir une échelle pour la concentration en CO2 
![CO2](https://images.staticjw.com/vcp/6394/co2-ppm-table.jpg)

Il va donc falloir faire:

* un peu d'informatique
* un peu d'électronique
* un peu de mécanique

Et si d'aventure, tout n'était pas fini, étant désormais membre du fablab,
vous pourrez revenir pour finir, refaire, améliorer, aider d'autres à en faire..

L'objet terminé devrait plus ou moins ressembler à ceci:

![objet](./images/capteur.jpg)

## Composants

On distingue 2 parties dans le capteur de CO2:

* le contenu = les composants électroniques programmés et reliés entre eux
* le contenant = l'objet inerte = la boîte

Le contenant est constitué de bois, de colle et de plastique.

Le contenu est constitué de 3 composants électroniques:

* le "cerveau" aussi appelé micro contrôleur
* le capteur: l'élément qui va mesurer la concentration en CO2
* des petites lumières appelées LEDs qui vont permettre de fournir l'indication
  visuelle à l'utilisateur

On peut représenter l'ensemble comme suit:

![archi](./images/archi1.png)

Pour info:

- le micro controleur:  [Wemos D1 mini](https://projetsdiy.fr/wemos-d1-mini-esp8266-test/)  5€
- le [capteur de CO2 keyestudio CCS811](https://www.keyestudio.com/products/keyestudio-ccs811-carbon-dioxide-temperature-air-quality-sensor-for-arduino) 10€
- les lumières [shield led neopixel](https://www.adafruit.com/product/1643)  2€
- le bois/le plastique le temps d'utilisation des machines 3€

soit un coût de revient de 20€.

## Présentation des sous ateliers

Nous allons réaliser l'objet à travers 3 mini ateliers où vous serez mis
à contribution:

* [atelier informatique](./wemos.md): on va écrire du code logiciel dans le "cerveau"
* [atelier électronique](./soudure.md): on va souder et relier les différents composants
* [atelier mécanique](./objet.md): on va fabriquer l'objet