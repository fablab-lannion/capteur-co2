# capteur-co2

Le but de ce projet est de fabriquer un capteur de CO2.
En fonction de la concentration en CO² on fait varier la couleur d'un led.

![CO2](https://images.staticjw.com/vcp/6394/co2-ppm-table.jpg)

Pour mieux distinguer les couleurs, on a choisi arbitrairement:

- ppm > 2000: Rouge 
- ppm > 1600: Orange 
- ppm > 1100: Jaune 
- ppm > 900: Vert clair
- ppm > 700: Vert
- autres: bleu 

Si on constate un changement d'état, on fait clignoter les leds.

## Composants

Pour ce capteur on utilise:

- un [Wemos D1 mini](https://projetsdiy.fr/wemos-d1-mini-esp8266-test/)  3-8€
- un [capteur de CO² keyestudio CCS811]() 8-10€
- un [shield led neopixel](neopixel)  2-10€

Attention, il faut utiliser la libbrairie du capteur dispo [ici](./arduino/lib/)
## Arduino

le code est [ici](./arduino/code.ino)

## Objet

Pour intégrer le montage électronique, on a choisi de faire un petit tardis.  

